#############################################
# BUILDER IMAGE: Only for building the code #
#############################################
FROM python:3.8-slim-buster AS builder

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apt-get update && apt-get install -y \
    gcc \
    python3-dev

# Create user for building and installing pip packages inside its home for security purposes
RUN useradd --create-home appbuilder
ENV BUILDER_HOME=/home/appbuilder
WORKDIR $BUILDER_HOME
USER appbuilder

# Install dependencies and create layer cache of them
COPY --chown=appbuilder requirements.txt .
RUN pip install --user -r requirements.txt

# Install test dependencies and create layer cache of them
COPY --chown=appbuilder test-requirements.txt .
RUN pip install --user -r test-requirements.txt

######################################
# RUNNER IMAGE: For running the code #
######################################
FROM python:3.8-slim-buster as production

# Install here only runtime required packages
RUN apt-get update && apt-get install -y \
    curl \
    jpegoptim \
    optipng

RUN groupadd -g 2000 appmanager && \
    useradd -u 2000 -g appmanager --create-home appmanager

ENV USER_HOME=/home/appmanager
WORKDIR $USER_HOME
USER appmanager

# Copy pip install results from builder image
COPY --from=builder --chown=appmanager /home/appbuilder/.local $USER_HOME/.local

# Make sure scripts installed by pip in .local are usable:
ENV PATH=$USER_HOME/.local/bin:$PATH

# Celery and other services folder
RUN mkdir tmp

# Project files
COPY --chown=appmanager src/ src/
COPY --chown=appmanager manage.py .
COPY --chown=appmanager scripts/ scripts/
