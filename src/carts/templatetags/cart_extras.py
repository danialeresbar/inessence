from django import template

register = template.Library()


@register.filter()
def product_quantity_format(quantity=1):
    """

    """

    return f"{quantity} {'Products' if quantity > 1 else 'Product'} added to your cart!"


@register.simple_tag
def calculate_total_product(price, quantity):
    """

    """

    return price * quantity

