from django.shortcuts import render, redirect
from django.views.generic import TemplateView

from src.carts.models import CartProducts
from src.carts.utils import get_or_create_shopping_cart, get_product


# Function Based Views for add-delete products to shopping-cart
class ShoppingCartView(TemplateView):
    """

    """

    template_name = 'shop/carts/cart_detail.html'

    def get_context_data(self, *args, **kwargs):
        context = super(ShoppingCartView, self).get_context_data(*args, **kwargs)
        cart = get_or_create_shopping_cart(self.request)
        context['cart'] = cart

        return context


def cart_products_add(request):
    """

    """

    cart = get_or_create_shopping_cart(request)
    product_quantity = int(request.POST.get('product_quantity', 1))
    product = get_product(request)
    CartProducts.objects.create_or_update_product(
        cart=cart,
        product=product,
        quantity=product_quantity
    )

    return render(
        request,
        'shop/carts/product_added.html',
        {
            'quantity': product_quantity,
            'product': product
        }
    )


def cart_products_delete(request):
    """

    """

    cart = get_or_create_shopping_cart(request)
    product = get_product(request)
    cart.products.remove(product)

    return redirect('shop-cart')
