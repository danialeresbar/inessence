from django.urls import path

from src.carts.views import ShoppingCartView, cart_products_add, cart_products_delete


urlpatterns = [
    path('cart', ShoppingCartView.as_view(), name='shop-cart'),
    path('cart/items/added', cart_products_add, name='shop-cart-items-added'),
    path('cart/items/remove', cart_products_delete, name='shop-cart-items-deleted')
]
