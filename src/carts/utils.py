from django.shortcuts import get_object_or_404

from src.carts.models import Cart
from src.products.models import Product


def get_or_create_shopping_cart(request):
    """

    """

    user = request.user if request.user.is_authenticated else None
    if user:
        cart, _ = Cart.objects.get_or_create(owner=user)

    else:
        cart_slug = request.session.get('cart_slug')
        cart = Cart.objects.filter(slug=cart_slug).first()

        if cart is None:
            cart = Cart.objects.create(owner=user)

    request.session['cart_slug'] = cart.slug

    return cart


def get_product(request):
    """

    """

    product_id = request.POST.get('product_id')
    product = get_object_or_404(Product, pk=product_id)

    return product
