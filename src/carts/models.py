from decimal import Decimal

from django.db import models
from django.db.models.signals import post_save
from django.utils.text import slugify
from uuid import uuid4

from src.customers.models import Customer
from src.main.models import BaseModel
from src.products.models import Product


CART_PRODUCTS = 'Products'
CART_SLUG = 'Slug'
CART_SUBTOTAL = 'Subtotal'
CART_SUBTOTAL_MAX_DIGITS = 15
CART_SUBTOTAL_DECIMAL_PLACES = 2
CART_TOTAL = 'Total'
CART_TOTAL_MAX_DIGITS = 20
CART_TOTAL_DECIMAL_PLACES = 2
CART_VERBOSE_NAME = 'Cart'
CART_VERBOSE_NAME_PLURAL = 'Carts'


class Cart(BaseModel):
    """

    """

    FEE = 0.05  # Commission

    owner = models.OneToOneField(
        Customer,
        blank=True,
        null=True,
        on_delete=models.CASCADE
    )

    products = models.ManyToManyField(
        Product,
        blank=True,
        through='CartProducts',
        verbose_name=CART_PRODUCTS
    )

    subtotal = models.DecimalField(
        max_digits=CART_SUBTOTAL_MAX_DIGITS,
        decimal_places=CART_SUBTOTAL_DECIMAL_PLACES,
        blank=True,
        null=True,
        verbose_name=CART_SUBTOTAL
    )

    total = models.DecimalField(
        max_digits=CART_TOTAL_MAX_DIGITS,
        decimal_places=CART_TOTAL_DECIMAL_PLACES,
        blank=True,
        null=True,
        verbose_name=CART_TOTAL
    )

    slug = models.SlugField(
        unique=True,
        verbose_name=CART_SLUG
    )

    class Meta:
        verbose_name = CART_VERBOSE_NAME
        verbose_name_plural = CART_VERBOSE_NAME_PLURAL

    def __str__(self):
        return f'{self.slug}'

    def products_related(self):
        return self.cartproducts_set.select_related('product')

    def update_subtotal(self):
        self.subtotal = sum([
            (cartproduct.product.price*cartproduct.quantity) for cartproduct in self.products_related()
        ])
        self.save()

    def update_total(self):
        self.update_subtotal()
        self.total = self.subtotal + (self.subtotal * Decimal(self.FEE))
        self.save()
    
    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(f'{str(uuid4())}-cart')
        super(Cart, self).save(*args, **kwargs)


class CartProductManager(models.Manager):
    """

    """

    def create_or_update_product(self, cart, product, quantity=1):
        obj, created = self.get_or_create(cart=cart, product=product)
        if not created:
            quantity = obj.quantity + quantity

        obj.update_quantity(quantity)
        return obj


class CartProducts(models.Model):
    """

    """

    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1, blank=True, null=True)

    objects = CartProductManager()

    def __str__(self):
        return self.product.name

    def update_quantity(self, quantity):
        self.quantity = quantity
        self.save()


def update_cart_total(sender, instance, *args, **kwargs):
    """
    Callback used to update total and subtotal prices
    for a cart as products are added or removed
    """

    instance.cart.update_total()


post_save.connect(update_cart_total, sender=CartProducts)
