from django.contrib import admin

from src.carts.models import Cart, CartProducts
from src.main.admin import BaseArchiveAdmin


class CartProductsInline(admin.TabularInline):
    model = CartProducts
    extra = 0
    verbose_name = 'Product'
    verbose_name_plural = 'Products'


@admin.register(Cart)
class CartAdmin(admin.ModelAdmin):
    """

    """

    readonly_fields = ('slug',) + BaseArchiveAdmin.readonly_fields
    inlines = (CartProductsInline, )

    fieldsets = (
        ('Control', {
            'fields': (BaseArchiveAdmin.fields, 'slug')
        }),
        ('Cart', {
            'fields': ('owner', ('subtotal', 'total'))
        })
    )

    list_display = ('slug', 'owner') + BaseArchiveAdmin.list_display
    list_filter = BaseArchiveAdmin.list_filter
    search_fields = ('owner__name',)
