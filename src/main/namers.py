from pathlib import Path


def alias(thumbnailer, thumbnail_options, source_filename, thumbnail_extension, **kwargs):
    """
    Generate filename based on thumbnail alias name (option ``THUMBNAIL_ALIASES``).
    For example: ``source_medium_large.jpg``
    """
    return f"{Path(source_filename).stem}-{thumbnail_options.get('ALIAS', '')}.{thumbnail_extension}"
