from django.shortcuts import render
from django.views.generic import TemplateView

from src.products.models import Category, Product


class FrontpageView(TemplateView):
    """

    """

    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super(FrontpageView, self).get_context_data()

        # Include new data in the context
        context['categories'] = Category.objects.all().order_by('-creation_date')[:3]
        context['featured'] = Product.objects.filter(featured=True).order_by('-creation_date')[:3]
        context['products'] = Product.objects.all().order_by('-id')[:6]

        return context


class ContactView(TemplateView):
    """

    """

    template_name = 'contact.html'


class AboutView(TemplateView):
    """

    """

    template_name = 'about.html'


def handler403(request, exception):
    return render(
        request,
        'errors/403.html'
    )


def handler404(request, exception):
    return render(
        request,
        'errors/404.html'
    )


def handler500(request):
    return render(
        request,
        'errors/500.html'
    )
