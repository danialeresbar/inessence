from django.urls import path
from django.urls import include
from src.main.views import AboutView, ContactView, FrontpageView

from rest_framework import routers
from src.main.api import ConfigurationView

# api urls
api_router = routers.DefaultRouter()
# /api/main/configurations
api_router.register('configurations', ConfigurationView)

apiurls = ([
    # /api/main/<routers>
    path('', include(api_router.urls))
], 'main')

# general urls
urlpatterns = [
    path('', FrontpageView.as_view(), name='home'),
    path('about/', AboutView.as_view(), name='about'),
    path('contact/', ContactView.as_view(), name='contact'),
]
