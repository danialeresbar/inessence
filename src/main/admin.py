from django.conf import settings
from django.contrib import admin
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _
from easy_thumbnails.templatetags.thumbnail import thumbnail_url

from src.main.models import Configuration


@admin.register(Configuration)
class ConfigurationAdmin(admin.ModelAdmin):
    list_display = ["key", "value"]


class BaseArchiveAdmin(admin.ModelAdmin):
    readonly_fields = ('creation_date', 'last_modified')
    list_display = ('creation_date', 'last_modified', 'is_active')
    list_editable = ('is_active',)
    list_filter = ('is_active',)
    fields = ('creation_date', 'last_modified', 'is_active')


class ThumbnailPreview:
    """
    This will add a thumbnail image, a fancy preview for your Django admin
    list page. Let's say you have a model that has an image field. With this
    helper you will be able to display a thumbnail in the admin list page.
    For example:
        class ProductAdmin(ThumbnailPreview, admin.ModelAdmin):
            list_display = ('name', 'thumbnail')
    By default we will assume that you have an image field named `image`.
    If that's not the case, you will have to customize things.
        class ProductAdmin(ThumbnailPreview, admin.ModelAdmin):
            list_display = ('name', 'preview')
            custom_thumbnail = {
                'image_field': 'photo',
            }
    Note how you can customize the image field name but also the thumbnail size
    by defining a `custom_thumbnail` dictionary.
    """

    def thumbnail(self, obj):
        template = u"""<img src="{url}"/>"""
        url = f"{settings.STATIC_URL}products/img/noimage.png"
        config = {'image_field': 'image'}
        custom_config = getattr(self, 'custom_thumbnail', {})
        config.update(custom_config)
        image = getattr(obj, config['image_field'], None)
        if image:
            url = thumbnail_url(image, 'thumbnail')

        return mark_safe(template.format(url=url))
    thumbnail.short_description = _('thumbnail')
    thumbnail.allow_tags = True
