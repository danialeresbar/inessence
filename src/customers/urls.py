from django.urls import re_path, path

from src.customers.views import LogoutView, ProfileDetailView, ShippingAddressCreateView, ShippingAddressDeleteView, ShippingAddressUpdateView, SignInView, SignUpView


urlpatterns = [
    re_path(r'^signin/$', SignInView.as_view(), name='customer-login'),
    re_path(r'^logout/$', LogoutView.as_view(), name='customer-logout'),
    re_path(r'^signup/$', SignUpView.as_view(), name='customer-signup'),
    path('profile/', ProfileDetailView.as_view(), name='customer-profile'),
    path('profile/address/add', ShippingAddressCreateView.as_view(), name='customer-address-add'),
    path('profile/address/update/<int:pk>/', ShippingAddressUpdateView.as_view(), name='customer-address-update'),
    path('profile/address/delete/<int:pk>/', ShippingAddressDeleteView.as_view(), name='customer-address-delete')
]
