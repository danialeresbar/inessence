from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from src.customers.models import Customer, ShippingAddress


class ShippingAddressStackedInline(admin.StackedInline):
    """

    """

    model = ShippingAddress
    extra = 1


@admin.register(Customer)
class CustomerAdmin(UserAdmin):
    """

    """

    inlines = (ShippingAddressStackedInline, )

    def has_add_permission(self, request):
        return request.user.is_staff

    def has_change_permission(self, request, obj=None):
        return request.user.is_staff

    def has_delete_permission(self, request, obj=None):
        return request.user.is_staff
