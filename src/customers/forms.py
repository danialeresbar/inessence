from django import forms
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm

from src.customers.models import Customer, ShippingAddress


EMAIL_LABEL = 'Email'
USERNAME_LABEL = 'Username'
PASSWORD_LABEL = 'Password'
PASSWORD_CONFIRMATION_LABEL = 'Confirm your password'


REGION_LABEL = 'Region'
CITY_LABEL = 'City'
NEIGHBORHOOD_LABEL = 'Neighborhood'
FIRST_ADDRESS_LABEL = 'Address'
ADDITIONAL_DATA_LABEL = 'Additional data'
CONTACT_PHONE_ADDRESS_LABEL = 'Contact phone'


class LoginForm(AuthenticationForm):
    """
    Form used to login new customers
    """

    username = forms.CharField(
        label=USERNAME_LABEL,
        required=True,
        widget=forms.TextInput(attrs={
            'class': 'form-control mt-1',
            'autofocus': True,
            'id': 'username',
            'placeholder': 'Username'
        })
    )
    password = forms.CharField(
        label=PASSWORD_LABEL,
        required=True,
        strip=False,
        widget=forms.PasswordInput(attrs={
            'class': 'form-control mt-1',
            'id': 'password',
            'placeholder': 'Password',
            'autocomplete': 'current-password'
        })
    )

    class Meta:
        model = Customer
        fields = ('username', 'password')


class SignUpForm(UserCreationForm):
    """
    Form used to register new customers
    """

    username = forms.CharField(
        label=USERNAME_LABEL,
        required=True,
        min_length=8,
        max_length=32,
        widget=forms.TextInput(attrs={
            'class': 'form-control mt-1',
            'id': 'username',
            'placeholder': 'Username'
        })
    )
    email = forms.EmailField(
        label=EMAIL_LABEL,
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'form-control mt-1',
            'id': 'email',
            'placeholder': 'example@mail.com'
        })
    )
    password1 = forms.CharField(
        label=PASSWORD_LABEL,
        required=True,
        widget=forms.PasswordInput(attrs={
            'class': 'form-control mt-1',
            'id': 'password1',
            'placeholder': 'Password'
        })
    )
    password2 = forms.CharField(
        label=PASSWORD_CONFIRMATION_LABEL,
        required=True,
        widget=forms.PasswordInput(attrs={
            'class': 'form-control mt-1',
            'id': 'password2',
            'placeholder': 'Repeat your password'
        })
    )

    class Meta:
        model = Customer
        fields = ('username', 'email', 'password1', 'password2')


class ShippingAddressForm(forms.ModelForm):
    """
    Form used for CRUD operations with the shipping address model
    """

    region = forms.CharField(
        label=REGION_LABEL,
        required=True,
        widget=forms.TextInput(attrs={
            'class': 'form-control mt-1',
            'autofocus': True,
            'id': 'region',
            'placeholder': 'Region'
        })
    )
    city = forms.CharField(
        label=CITY_LABEL,
        required=True,
        widget=forms.TextInput(attrs={
            'class': 'form-control mt-1',
            'id': 'city',
            'placeholder': 'City'
        })
    )
    neighborhood = forms.CharField(
        label=NEIGHBORHOOD_LABEL,
        required=False,
        max_length=64,
        widget=forms.TextInput(attrs={
            'class': 'form-control mt-1',
            'id': 'neighborhood',
            'placeholder': 'Neighborhood (optional)'
        })
    )
    address = forms.CharField(
        label=FIRST_ADDRESS_LABEL,
        required=True,
        max_length=64,
        widget=forms.TextInput(attrs={
            'class': 'form-control mt-1',
            'id': 'address',
            'placeholder': 'Address'
        })
    )
    additional_data = forms.CharField(
        label=ADDITIONAL_DATA_LABEL,
        required=False,
        max_length=32,
        widget=forms.TextInput(attrs={
            'class': 'form-control mt-1',
            'id': 'additional',
            'placeholder': 'Additional data (optional)'
        })
    )
    contact_phone = forms.CharField(
        label=CONTACT_PHONE_ADDRESS_LABEL,
        required=False,
        max_length=16,
        widget=forms.TextInput(attrs={
            'class': 'form-control mt-1',
            'id': 'contact',
            'placeholder': 'Telephone contact'
        })
    )

    class Meta:
        model = ShippingAddress
        fields = ('region', 'city', 'neighborhood', 'address', 'additional_data', 'contact_phone')
