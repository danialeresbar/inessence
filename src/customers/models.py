from django.contrib.auth.models import AbstractUser
from django.db import models


CUSTOMER_SINGULAR = 'Customer'
CUSTOMER_PLURAL = 'Customers'
CUSTOMER_BIO = 'Bio'
CUSTOMER_GENDER = 'Gender'
CUSTOMER_GENDER_MAX_LENGTH = 32
GENDER_CHOICES = [
    ('F', 'Female'),
    ('M', 'Male'),
    ('-', 'Prefer Not To Say')
]
CUSTOMER_PHONE_NUMBER = 'Phone number'
CUSTOMER_PHONE_NUMBER_MAX_LENGTH = 16
CUSTOMER_AVATAR = 'Avatar'


SHIPPING_ADDRESS_CUSTOMER = 'Customer'
SHIPPING_ADDRESS_CUSTOMER_RELATED_NAME = 'addresses'
SHIPPING_ADDRESS_CONTACT_PHONE = 'Contact phone'
SHIPPING_ADDRESS_CONTACT_PHONE_MAX_LENGTH = 16
SHIPPING_ADDRESS_REGION = 'Region'
SHIPPING_ADDRESS_REGION_MAX_LENGTH = 64
SHIPPING_ADDRESS_CITY = 'City'
SHIPPING_ADDRESS_CITY_MAX_LENGTH = 64
SHIPPING_ADDRESS_NEIGHBORHOOD = 'Neighborhood'
SHIPPING_ADDRESS_NEIGHBORHOOD_MAX_LENGTH = 64
SHIPPING_ADDRESS_MAIN = 'Address'
SHIPPING_ADDRESS_MAX_LENGTH = 64
SHIPPING_ADDRESS_POSTAL_CODE = 'Postal code'
SHIPPING_ADDRESS_POSTAL_CODE_MAX_LENGTH = 16
SHIPPING_ADDRESS_ADDITIONAL_DATA = 'Additional data'
SHIPPING_ADDRESS_ADDITIONAL_DATA_MAX_LENGTH = 64
SHIPPING_ADDRESS_VERBOSE_NAME = 'Shipping address'
SHIPPING_ADDRESS_VERBOSE_NAME_PLURAL = 'Shipping addresses'


class Customer(AbstractUser):
    """
    Custom class with the attributes intended for a client
    """

    picture = models.ImageField(upload_to='customers/avatars/', blank=True, null=True, verbose_name=CUSTOMER_AVATAR)
    bio = models.TextField(blank=True, null=True, verbose_name=CUSTOMER_BIO)
    phone_number = models.CharField(
        max_length=CUSTOMER_PHONE_NUMBER_MAX_LENGTH,
        blank=True,
        null=True,
        verbose_name=CUSTOMER_PHONE_NUMBER
    )
    gender = models.CharField(
        max_length=CUSTOMER_GENDER_MAX_LENGTH,
        choices=GENDER_CHOICES,
        default=GENDER_CHOICES[2][1],
        verbose_name=CUSTOMER_GENDER
    )

    class Meta:
        verbose_name = CUSTOMER_SINGULAR
        verbose_name_plural = CUSTOMER_PLURAL

    def __str__(self):
        return self.username

    def get_addresses(self):
        return self.addresses.all() if self.addresses else []

    def get_personal_info(self):
        return {
            'Name': self.get_full_name(),
            'Email': self.email,
            'Telephone': self.phone_number,
            'Gender': self.gender
        }


class ShippingAddress(models.Model):
    """
    Class used to model the shipping addresses of a customer
    """

    customer = models.ForeignKey(
        Customer,
        on_delete=models.CASCADE,
        related_name=SHIPPING_ADDRESS_CUSTOMER_RELATED_NAME,
        verbose_name=SHIPPING_ADDRESS_CUSTOMER
    )
    region = models.CharField(max_length=SHIPPING_ADDRESS_REGION_MAX_LENGTH, verbose_name=SHIPPING_ADDRESS_REGION)
    city = models.CharField(max_length=SHIPPING_ADDRESS_CITY_MAX_LENGTH, verbose_name=SHIPPING_ADDRESS_CITY)
    neighborhood = models.CharField(
        max_length=SHIPPING_ADDRESS_NEIGHBORHOOD_MAX_LENGTH,
        blank=True,
        null=True,
        verbose_name=SHIPPING_ADDRESS_NEIGHBORHOOD
    )
    address = models.CharField(max_length=SHIPPING_ADDRESS_MAX_LENGTH, verbose_name=SHIPPING_ADDRESS_MAIN)
    additional_data = models.CharField(
        max_length=SHIPPING_ADDRESS_ADDITIONAL_DATA_MAX_LENGTH,
        blank=True,
        null=True,
        verbose_name=SHIPPING_ADDRESS_ADDITIONAL_DATA
    )
    contact_phone = models.CharField(
        max_length=SHIPPING_ADDRESS_CONTACT_PHONE_MAX_LENGTH,
        blank=True,
        null=True,
        verbose_name=SHIPPING_ADDRESS_CONTACT_PHONE
    )

    class Meta:
        verbose_name = SHIPPING_ADDRESS_VERBOSE_NAME
        verbose_name_plural = SHIPPING_ADDRESS_VERBOSE_NAME_PLURAL

    def __str__(self):
        return f'{self.customer} - {self.address}'
