from django.contrib.auth import login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.http import Http404
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import DetailView, FormView, ListView, RedirectView
from django.views.generic.edit import CreateView, DeleteView, UpdateView

from src.customers.forms import LoginForm, ShippingAddressForm, SignUpForm
from src.customers.models import ShippingAddress


ADDRESS_CONTEXT_OBJECT_NAME = 'addresses'
PROFILE_CONTEXT_OBJECT_NAME = 'customer'


class SignInView(FormView):
    """
    Controller for customer login
    """

    template_name = 'customers/registration/signin.html'
    form_class = LoginForm
    success_url = reverse_lazy('customer-profile')

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            if 'next' in request.GET:
                return redirect(request.GET.get('next'))
            return redirect(self.success_url)
        else:
            return super(SignInView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        login(self.request, form.get_user())
        if 'next' in self.request.GET:
            return redirect(self.request.GET.get('next'))
        return super(SignInView, self).form_valid(form)


class SignUpView(CreateView, SuccessMessageMixin):
    """
    Controller for customer sign up
    """

    template_name = 'customers/registration/signup.html'
    success_url = reverse_lazy('customer-login')
    form_class = SignUpForm
    success_message = ''


class LogoutView(RedirectView):
    """
    Controller for customer logout
    """

    pattern_name = 'home'

    def get(self, request, *args, **kwargs):
        logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)


class ProfileDetailView(LoginRequiredMixin, DetailView):
    """
    Controller for the detail view of a customer's profile
    """

    template_name = 'customers/profile/profile_detail.html'
    login_url = reverse_lazy('customer-login')
    context_object_name = PROFILE_CONTEXT_OBJECT_NAME

    def get_object(self, queryset=None):
        if hasattr(self.request, 'user'):
            return self.request.user
        else:
            raise Http404("An anonymous customer cannot have a profile")


class ProfileUpdateView(LoginRequiredMixin, UpdateView):
    """
    Controller for updating a customer's profile data
    """

    template_name = 'customers/profile/profile_detail.html'
    login_url = reverse_lazy('customer-login')
    success_url = reverse_lazy('customer-profile')


class ShippingAddressCreateView(LoginRequiredMixin, CreateView):
    """
    Controller for creating a shipping address
    """

    form_class = ShippingAddressForm
    template_name = 'customers/shipping_addresses/snippets/add_address.html'
    login_url = reverse_lazy('customer-login')
    success_url = reverse_lazy('customer-profile')
    
    def form_valid(self, form):
        customer = self.request.user
        shipping_address = form.save(commit=False)
        shipping_address.customer = customer
        return super(ShippingAddressCreateView, self).form_valid(form)


class ShippingAddressUpdateView(LoginRequiredMixin, UpdateView):
    """
    Controller for updating a shipping address
    """

    model = ShippingAddress
    form_class = ShippingAddressForm
    template_name = 'customers/shipping_addresses/snippets/update_address.html'
    login_url = reverse_lazy('customer-login')
    success_url = reverse_lazy('customer-profile')


class ShippingAddressDeleteView(LoginRequiredMixin, DeleteView):
    """
    Controller for deleting a shipping address
    """

    model = ShippingAddress
    login_url = reverse_lazy('customer-login')
    success_url = reverse_lazy('customer-profile')
