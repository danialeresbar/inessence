from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import TemplateView

from src.carts.utils import get_or_create_shopping_cart
from src.orders.utils import breadcrumb, get_or_create_shopping_order


ADDRESS_CONTEXT_OBJECT_NAME = 'addresses'


class OrderSummaryTemplate(LoginRequiredMixin, TemplateView):
    """

    """

    template_name = 'shop/orders/order.html'
    login_url = reverse_lazy('customer-login')

    def get_context_data(self, *args, **kwargs):
        context = super(OrderSummaryTemplate, self).get_context_data(*args, **kwargs)
        cart = get_or_create_shopping_cart(self.request)
        order = get_or_create_shopping_order(cart, self.request)
        context['cart'] = cart
        context['order'] = order
        context['breadcrumb'] = breadcrumb()

        return context
