from enum import Enum
from uuid import uuid4

from django.db import models
from django.utils.text import slugify

from src.carts.models import Cart
from src.customers.models import Customer
from src.main.models import BaseModel


ORDER_CUSTOMER = 'Customer'
ORDER_CUSTOMER_RELATED_NAME = 'orders'
ORDER_CART = 'Shopping cart'
ORDER_STATUS = 'Status'
ORDER_STATUS_MAX_LENGTH = 16
ORDER_SHIPPING_FEE = 'Shipping fee'
ORDER_SHIPPING_FEE_DEFAULT = 5
ORDER_SHIPPING_FEE_DECIMAL_PLACES = 2
ORDER_SHIPPING_FEE_MAX_DIGITS = 8
ORDER_SLUG = 'Slug'
ORDER_TOTAL = 'Total'
ORDER_TOTAL_DEFAULT = 0
ORDER_TOTAL_DECIMAL_PLACES = 2
ORDER_TOTAL_MAX_DIGITS = 10
ORDER_VERBOSE_NAME = 'Order'
ORDER_VERBOSE_NAME_PLURAL = 'Orders'


class OrderStatus(Enum):
    CREATED = 'CREATED'
    PAYED = 'PAYED'
    COMPLETED = 'COMPLETED'
    CANCELED = 'CANCELED'


order_choices = [(tag, tag.value) for tag in OrderStatus]


class Order(BaseModel):
    """

    """

    customer = models.ForeignKey(
        Customer,
        on_delete=models.CASCADE,
        related_name=ORDER_CUSTOMER_RELATED_NAME,
        verbose_name=ORDER_CUSTOMER
    )
    cart = models.OneToOneField(Cart, on_delete=models.CASCADE, verbose_name=ORDER_CART)
    status = models.CharField(max_length=ORDER_STATUS_MAX_LENGTH, verbose_name=ORDER_STATUS, choices=order_choices)
    shipping_fee = models.DecimalField(
        max_digits=ORDER_SHIPPING_FEE_MAX_DIGITS,
        decimal_places=ORDER_SHIPPING_FEE_DECIMAL_PLACES,
        default=ORDER_SHIPPING_FEE_DEFAULT,
        verbose_name=ORDER_SHIPPING_FEE
    )
    total = models.DecimalField(
        max_digits=ORDER_TOTAL_MAX_DIGITS,
        decimal_places=ORDER_TOTAL_DECIMAL_PLACES,
        default=ORDER_TOTAL_DEFAULT,
        verbose_name=ORDER_TOTAL
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=ORDER_SLUG
    )

    class Meta:
        verbose_name = ORDER_VERBOSE_NAME
        verbose_name_plural = ORDER_VERBOSE_NAME_PLURAL

    def __str__(self):
        return f'{self.customer} order in {self.cart}'

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(f'{str(uuid4())}-order')
