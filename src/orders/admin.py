from django.contrib import admin

from src.main.admin import BaseArchiveAdmin
from src.orders.models import Order


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    """

    """

    readonly_fields = ('slug',) + BaseArchiveAdmin.readonly_fields

    fieldsets = (
        ('Control', {
            'fields': (BaseArchiveAdmin.fields, 'slug')
        }),
        ('Order', {
            'fields': (('customer', 'status'), 'cart', 'shipping_fee', 'total')
        })
    )

    list_display = ('id', 'customer', 'total', 'status')
    list_editable = ('status',)
    list_filter = ('status', 'creation_date', 'last_modified')
