from src.orders.models import Order


def get_or_create_shopping_order(cart, request):
    """

    """

    customer = request.user
    order = Order.objects.filter(cart=cart).first()

    if not order and customer.is_authenticated:
        order = Order.objects.create(cart=cart, customer=customer)

    if order:
        request.session['order_slug'] = order.slug

    return order


def breadcrumb(cart=True, order=False, address=False, confirmation=False):
    """

    """

    breadcrumb_items = [
        {
            'label': 'Cart',
            'active': cart,
            'url': ''
        },
        {
            'label': 'Order',
            'active': order,
            'url': ''
        },
        {
            'label': 'Address and payment',
            'active': address,
            'url': ''
        },
        {
            'label': 'Confirmation',
            'active': confirmation,
            'url': ''
        }
    ]

    return breadcrumb_items
