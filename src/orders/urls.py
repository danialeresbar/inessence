from django.urls import path

from src.orders.views import OrderSummaryTemplate


urlpatterns = [
    path('order/summary/', OrderSummaryTemplate.as_view(), name='shop-order'),
]

