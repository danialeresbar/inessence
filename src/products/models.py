from django.db import models
from django.urls import reverse
from django.utils.text import slugify
from easy_thumbnails.fields import ThumbnailerImageField

from src.main.models import BaseModel


CATEGORY_IMAGE = 'Image'
CATEGORY_IMAGE_PATH = 'products/categories/'
CATEGORY_NAME = 'Name'
CATEGORY_NAME_MAX_LENGTH = 32
CATEGORY_DESCRIPTION = 'Description'
CATEGORY_SLUG = 'Slug'
CATEGORY_VERBOSE_NAME = 'Category'
CATEGORY_VERBOSE_NAME_PLURAL = 'Categories'


PRODUCT_CATEGORIES = 'Categories'
PRODUCT_CATEGORIES_RELATED_NAME = 'products'
PRODUCT_DESCRIPTION = 'Description'
PRODUCT_FEATURED = 'Featured'
PRODUCT_IMAGE = 'Image'
PRODUCT_IMAGE_PATH = 'products/products/'
PRODUCT_NAME = 'Name'
PRODUCT_NAME_MAX_LENGTH = 64
PRODUCT_PRICE = 'Price'
PRODUCT_PRICE_DECIMAL_PLACES = 2
PRODUCT_PRICE_MAX_DIGITS = 8
PRODUCT_SLUG = 'Slug'
PRODUCT_VERBOSE_NAME = 'Product'
PRODUCT_VERBOSE_NAME_PLURAL = 'Products'


class Category(BaseModel):
    """

    """

    name = models.CharField(
        max_length=CATEGORY_NAME_MAX_LENGTH,
        verbose_name=CATEGORY_NAME
    )

    description = models.TextField(blank=True, verbose_name=CATEGORY_DESCRIPTION)

    image = ThumbnailerImageField(
        upload_to=CATEGORY_IMAGE_PATH,
        null=True,
        blank=True,
        verbose_name=CATEGORY_IMAGE
    )

    slug = models.SlugField(
        unique=True,
        verbose_name=CATEGORY_SLUG
    )

    class Meta:
        verbose_name = CATEGORY_VERBOSE_NAME
        verbose_name_plural = CATEGORY_VERBOSE_NAME_PLURAL

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Category, self).save(*args, **kwargs)


class Product(BaseModel):
    """

    """

    name = models.CharField(
        max_length=PRODUCT_NAME_MAX_LENGTH,
        verbose_name=PRODUCT_NAME
    )

    featured = models.BooleanField(default=False, verbose_name=PRODUCT_FEATURED)

    description = models.TextField(verbose_name=PRODUCT_DESCRIPTION, blank=True)

    price = models.DecimalField(
        decimal_places=PRODUCT_PRICE_DECIMAL_PLACES,
        max_digits=PRODUCT_PRICE_MAX_DIGITS,
        verbose_name=PRODUCT_PRICE
    )

    image = ThumbnailerImageField(
        upload_to=PRODUCT_IMAGE_PATH,
        null=True,
        blank=True,
        verbose_name=PRODUCT_IMAGE
    )

    categories = models.ManyToManyField(
        Category,
        blank=True,
        related_name=PRODUCT_CATEGORIES_RELATED_NAME,
        verbose_name=PRODUCT_CATEGORIES
    )

    slug = models.SlugField(
        verbose_name=PRODUCT_SLUG,
        unique=True
    )

    class Meta:
        verbose_name = PRODUCT_VERBOSE_NAME
        verbose_name_plural = PRODUCT_VERBOSE_NAME_PLURAL

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('product_detail', args=[self.slug])

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Product, self).save(*args, **kwargs)
