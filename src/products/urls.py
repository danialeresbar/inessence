from django.urls import path

from src.products.views import ProductDetailView, ProductListView


urlpatterns = [
    path('products/', ProductListView.as_view(), name='shop'),
    path('product/<slug:slug>/', ProductDetailView.as_view(), name='product_detail'),
]
