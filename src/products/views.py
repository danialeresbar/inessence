from django.views.generic import DetailView, ListView

from src.products.models import Product


PRODUCT_CONTEXT_OBJECT_NAME = 'products'


class ProductListView(ListView):
    """
    
    """

    template_name = 'shop/shop.html'
    queryset = Product.objects.all().order_by('-creation_date')
    context_object_name = PRODUCT_CONTEXT_OBJECT_NAME
    paginate_by = 6

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['message'] = 'Product list'
        return context

    def get_queryset(self):
        query = self.request.GET.get('q', None)
        if query:
            queryset = Product.objects.filter(name__icontains=query)
            return queryset
        else:
            return self.queryset


class ProductDetailView(DetailView):
    """

    """

    model = Product
    template_name = 'shop/products/product_detail.html'
