from django.conf import settings
from django.contrib import admin
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _
from easy_thumbnails.templatetags.thumbnail import thumbnail_url

from src.main.admin import BaseArchiveAdmin, ThumbnailPreview
from src.products.models import Category, Product


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin, ThumbnailPreview):
    """

    """

    change_form_template = 'shop/admin/change_form.html'
    readonly_fields = ('slug',) + BaseArchiveAdmin.readonly_fields
    fieldsets = (
        ('', {
            'fields': (BaseArchiveAdmin.fields, 'slug')
        }),
        ('Category', {
            'fields': ('name', 'image', 'description')
        })
    )

    list_display = ('id', 'name', 'thumbnail') + BaseArchiveAdmin.list_display
    list_editable = ('name',) + BaseArchiveAdmin.list_editable
    list_filter = BaseArchiveAdmin.list_filter


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin, ThumbnailPreview):
    """

    """

    change_form_template = 'shop/admin/change_form.html'
    readonly_fields = ('slug',) + BaseArchiveAdmin.readonly_fields
    fieldsets = (
        ('Control', {
            'fields': (BaseArchiveAdmin.fields, 'slug')
        }),
        ('Product', {
            'fields': (('name', 'price'), 'featured', 'image', 'description', 'categories')
        })
    )

    filter_horizontal = ('categories',)
    list_display = ('id', 'name', 'thumbnail', 'featured', 'price') + BaseArchiveAdmin.list_display
    list_editable = ('name', 'featured', 'price') + BaseArchiveAdmin.list_editable
    list_filter = ('featured',) + BaseArchiveAdmin.list_filter
