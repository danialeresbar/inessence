from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from src.main.urls import apiurls as main_apiurls


# Create the API namespace and add the API only URLs of the applications
apiurls = ([
    path('main/', include(main_apiurls, namespace='main')),
], 'api')

urlpatterns = [
    path('', include('src.main.urls')),
    path('customers/', include('src.customers.urls')),
    path('shop/', include('src.products.urls')),
    path('shop/', include('src.carts.urls')),
    path('shop/', include('src.orders.urls')),
    path('api/', include(apiurls, namespace='api')),
    path('admin/', admin.site.urls),
]

if not settings.IS_PRODUCTION:
    import debug_toolbar
    urlpatterns += [
        path('__debug__/', include(debug_toolbar.urls)),
    ]
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

handler403 = 'src.main.views.handler403'
handler404 = 'src.main.views.handler404'
handler500 = 'src.main.views.handler500'
