import os
from pathlib import Path

ENVIRONMENT = os.environ.get('ENVIRONMENT', 'development')
IS_PRODUCTION = ENVIRONMENT == "production"

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

# Application definition

DEPENDENCIES_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_cleanup.apps.CleanupConfig',
]

PROJECT_APPS = [
    'src.main',
    'src.carts',
    'src.customers',
    'src.products',
    'src.orders',
]

ADDONS = [
    'rest_framework',
    'easy_thumbnails',
    'easy_thumbnails.optimize'
]

INSTALLED_APPS = DEPENDENCIES_APPS + PROJECT_APPS + ADDONS

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'src.urls'

AUTH_USER_MODEL = 'customers.Customer'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'debug': True,
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'src.wsgi.application'

# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    )
}


# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'America/Bogota'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

# New Django settings
DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

# Email Config
EMAIL_SUBJECT_PREFIX = '[inessence] '
DEFAULT_FROM_EMAIL = 'No Reply <no-reply@inessence.com>'
SERVER_EMAIL = 'Server <server@inessence.com>'
# ADMINS = [('Admin', 'admin@inessence.com')]
# MANAGERS = [('Admin', 'admin@inessence.com')]


# Celery settings


# Easy-thumbnails settings

THUMBNAIL_CACHE_DIMENSIONS = False

THUMBNAIL_DEBUG = True

THUMBNAIL_NAMER = 'src.main.namers.alias'

THUMBNAIL_OPTIMIZE_COMMAND = {
    'png': '/usr/bin/optipng {filename}',
    'gif': '/usr/bin/optipng {filename}',
    'jpeg': '/usr/bin/jpegoptim {filename}'
}

THUMBNAIL_ALIASES = {
    'customers': {
        'screen': {
            'size': (80, 80),
            'folder': 'thumbs'
        },
        'mini': {'size': (170, 170)},
        'thumbnail': {'size': (120, 120)}
    },
    'products': {
        'large': {'size': (900, 900), 'crop': True},
        'medium': {'size': (600, 800), 'crop': True},
        'small': {'size': (320, 320), 'crop': True},
        'thumbnail': {'size': (60, 60), 'crop': True},
        'featured': {'size': (600, 450), 'crop': True},
        'preview': {'size': (130, 130), 'crop': True}
    }
}

THUMBNAIL_SUBDIR = 'variants'
