#!/bin/sh

python manage.py migrate
python manage.py loaddata initial categories customers products
python manage.py collectstatic --noinput
