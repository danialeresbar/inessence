# Inessence

Django 3.2 + Postgis 12 + Dokku and Heroku config (Production Ready)

## Documentation ##

This application is an online store of a small family business.

### Directory Tree ###
```
Soon
```

## Development

### How to run the project ###

The project use docker, so just run:

```
docker-compose up
```

> If it's first time, the images will be created. Sometimes the project doesn't run at first time because the init of postgres, just run again `docker-compose up` and it will work.

*Your app will run in url `localhost:8005`*

To recreate the docker images after dependencies changes run:

```
docker-compose up --build
```

To remove the docker containers including database (Useful sometimes when dealing with migrations):

```
docker-compose down
```

### Accessing Administration

The django admin site of the project can be accessed at `localhost:8005/admin`

By default, the development configuration creates a superuser with the following credentials:

```
Username: admin
Password: admin
```

## Production Deployment: Dokku

The project is dokku ready, these are the steps to deploy it in your dokku server:

#### Server Side: ####

> This doc does not cover dokku setup, you should already have configured the initial dokku config including ssh keys

Create app and configure postgres:
```
dokku apps:create inessence
dokku postgres:create inessence
dokku postgres:link inessence inessence
```

> If you don't have dokku postgres installed, run this before:
> `sudo dokku plugin:install https://github.com/dokku/dokku-postgres.git`

Create the required environment variables:
```
dokku config:set inessence ENVIRONMENT=production DJANGO_SECRET_KEY=....
```

Current required environment variables are:

* ENVIRONMENT
* DJANGO_SECRET_KEY
* EMAIL_PASSWORD

Use the same command to configure secret credentials for the app

#### Local Side: ####

Configure the dokku remote:

```
git remote add production dokku@<my-dokku-server.com>:inessence
```

Push your changes and just wait for the magic to happens :D:

```
git push production master
```

Optional: To add SSL to the app check:
https://github.com/dokku/dokku-letsencrypt

Optional: Additional nginx configuration (like client_max_body_size) should be placed server side in:
```
/home/dokku/<app>/nginx.conf.d/<app>.conf
```

> Further dokku configuration can be found here: http://dokku.viewdocs.io/dokku/

## Production Deployment: Heroku

The project is Heroku ready with
**[Build Manifest](https://devcenter.heroku.com/articles/build-docker-images-heroku-yml)**
deploy approach. You should follow these steps to deploy it as heroku app:
> Keep in mind Docker-based deployments are limited to the same constraints that Git-based
> deployments are. For example, persistent volumes are not supported since the file system
> is ephemeral and web processes only support HTTP(S) requests.

### Prerequisites 📋 ###

The production environment requires certain configuration before deploying the docker image,
such as the database, the AWS and the CELERY settings.

### Environment variables 🛠️ ###

The system is configured using environment variables. The following is the list of
environment variables that are required or optional before deploying the system:

| Variable | Description | Required | Default |
| :--- | :--- | :---: | :--- |
| `DJANGO_SECRET_KEY` | Key used by Django for tokens like CSRF and cookies, it can be any secret key but it's recommended to generate it using https://djecrety.ir/ | **yes** | *None* |
| `RABBITMQ_USER` | Custom username for the Rabbitmq broker | **yes** | *None* |
| `RABBITMQ_PASS` | Custom password for the Rabbitmq broker | **yes** | *None* |
| `ENVIRONMENT` | Project environment settings | **no** | *development* |
| `USE_S3` | Used to turn the S3 storage on | **no** | *True* |
| `AWS_ACCESS_KEY_ID` | Your Amazon Web Services access key, as a string | **yes** | *None* |
| `AWS_SECRET_KEY_ACCESS` | Your Amazon Web Services secret access key, as a string | **yes** | *None* |
| `AWS_STORAGE_BUCKET_NAME` | Your Amazon Web Services storage bucket name, as a string | **yes** | *inessencebucket* |
| `AWS_REGION` | Specifies the AWS Region to send the request to | **no** | *None* |

### Backing services ⚙️ ###

As expected in a Twelve Factors App the following services needs to be configured
using environment variables as well:

| Service | Environment variable | Value | Example |
| :--- | :---: | :--- | :--- |
| Postgres Database | `DATABASE_URL` | `postgresql://<user>:<pass>@<host>:<port>/<dbname>` | `postgres://dlfgyvooqebjiq:7f5a5bfbedf60019262c16dbfa78ea1558e48f7977cb8bc91de670ff0aeeeb02@ec2-18-233-83-165.compute-1.amazonaws.com:5432/d88kfm43j69i0s` |

### Deployment ☁ ###

When having all the prerequisites and, you have logged in with your Heroku client, 
clone the repository in the server, then deploy the containers with the commands:

```
heroku update beta
heroku plugins:install @heroku-cli/plugin-manifest
```

Then create your app using the --manifest flag. The stack of the app will
automatically be set to container:

```
heroku create appname --manifest
```

> Do not forget change appname for your app name

Commit your [heroku.yml](heroku.yml) to git:

```
git add heroku.yml
git commit -m "Add the heroku build manifest"
```

Push the code:

```
git push heroku master
```

> Please check the [Known issues and limitations](https://devcenter.heroku.com/articles/build-docker-images-heroku-yml#known-issues-and-limitations)
> for this approach

Finally, your application can be accessed from the Heroku [dashboard](https://dashboard.heroku.com/apps) 🚀

