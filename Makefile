superuser:
	docker exec -it inessence ./manage.py createsuperuser

bash:
	docker exec -it inessence bash

shell:
	docker exec -it inessence ./manage.py shell

makemigrations:
	docker exec -it inessence ./manage.py makemigrations

showmigrations:
	docker exec -it inessence ./manage.py showmigrations

migrate:
	docker exec -it inessence ./manage.py migrate

initialfixture:
	docker exec -it inessence ./manage.py loaddata initial

testfixture:
	docker exec -it inessence ./manage.py loaddata test

test:
	docker exec -it inessence ./manage.py test

statics:
	docker exec -it inessence ./manage.py collectstatic --noinput

makemessages:
	docker exec -it inessence django-admin makemessages

compilemessages:
	docker exec -it inessence django-admin compilemessages
